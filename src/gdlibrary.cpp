#include "board.h"
#include "shop_item.h"
#include "unit.h"
#include "team_unit.h"
#include "battlefield.h"
#include "main_menu.h"
#include "game.h"
#include "game_manager.h"

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);

    godot::register_class<godot::Board>();
    godot::register_class<godot::ShopItem>();
    godot::register_class<godot::Unit>();
    godot::register_class<godot::TeamUnit>();
    godot::register_class<godot::Battlefield>();
    godot::register_class<godot::MainMenu>();
    godot::register_class<godot::Game>();
    godot::register_class<godot::GameManager>();
}