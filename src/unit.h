#ifndef UNIT_H
#define UNIT_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>
#include <Label.hpp>
#include <TextureRect.hpp>
#include <vector>
#include <tuple>
#include <AnimationPlayer.hpp>

namespace godot {

class Unit : public Control {
    GODOT_CLASS(Unit, Control)

private:
    GameManager *game_manager;
    int id;
    int tier;
    int attack;
    int health;
    String name;
    Label *attack_label;
    Label *health_label;
    TextureRect *icon;
    std::vector<std::tuple<int, int, String>> unit_database;
    AnimationPlayer *animation_player;

public:
    static void _register_methods();

    Unit();

    void _init();

    void _ready();

    void load(int id);

    int get_id();

    std::tuple<int, int, String> get_data();

    void take_damage(int dmg);

    void update_health();

    void set_enemy_style();

    void home_attack_animate();

    void away_attack_animate();
};

}

#endif