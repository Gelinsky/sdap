#include "main_menu.h"

#include <SceneTree.hpp>

using namespace godot;

void MainMenu::_register_methods() {
    register_method("_ready", &MainMenu::_ready);
    register_method("_pressed_host", &MainMenu::_pressed_host);
    register_method("_pressed_join", &MainMenu::_pressed_join);
    register_method("_peer_connected", &MainMenu::_peer_connected);
}

MainMenu::MainMenu() {
}

void MainMenu::_init() {
}

void MainMenu::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    host_button = Object::cast_to<Button>(get_node("VBoxContainer/Buttons/VBoxContainer/Host"));
	host_button->connect("pressed", this, "_pressed_host");
    join_button = Object::cast_to<Button>(get_node("VBoxContainer/Buttons/VBoxContainer/Join"));
	join_button->connect("pressed", this, "_pressed_join");
    get_tree()->connect("network_peer_connected", this, "_peer_connected");
}

void MainMenu::_pressed_host() {
    net = NetworkedMultiplayerENet::_new();
	net->create_server(7070, 2);
	get_tree()->set_network_peer(net);
	host_button->set_text("Hosting...");
	host_button->set_disabled(true);
	join_button->set_disabled(true);
}

void MainMenu::_pressed_join() {
	net = NetworkedMultiplayerENet::_new();
	net->create_client("127.0.0.1", 7070);
	get_tree()->set_network_peer(net);
}

void MainMenu::_peer_connected(int64_t id) {
	game_manager->player_ids.append(id);
	get_tree()->change_scene("res://Game.tscn");
}
