#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <Godot.hpp>
#include <Control.hpp>
#include <vector>
#include <tuple>

namespace godot {

class TeamUnit; // forward declaration

class GameManager : public Control {
    GODOT_CLASS(GameManager, Control)

private:

public:
    std::vector<std::tuple<int, int, String, int>> unit_database;

    int animal_count;

    Array player_ids;

    Array enemy_ids;

    TeamUnit *selected_unit;

    int gold;

    int player_health;

    int turn;

    int current_tier;

    static void _register_methods();

    GameManager();

    void _init();

    void _ready();
};

}

#endif