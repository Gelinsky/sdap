#ifndef GAME_H
#define GAME_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>

namespace godot {

class Game : public Control {
    GODOT_CLASS(Game, Control)

private:
    GameManager* game_manager;
    // Array enemy_team;

public:
    static void _register_methods();

    Game();

    void _init();

    void _ready();

    void end_turn(Array unit_ids);

    void set_enemy_team(Array unit_ids, bool ready);
};

}

#endif