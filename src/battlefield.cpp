#include "battlefield.h"
#include "unit.h"
#include "team_unit.h"
#include "board.h"

#include <PackedScene.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>

using namespace godot;

void Battlefield::_register_methods() {
    register_method("_ready", &Battlefield::_ready);
    register_method("battle_tick", &Battlefield::battle_tick);
    register_method("_pressed_return", &Battlefield::_pressed_return);
}

Battlefield::Battlefield() {
}

void Battlefield::_init() {
}

void Battlefield::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    end_panel = Object::cast_to<Panel>(get_node("EndPanel"));
    win_loss_label = Object::cast_to<Label>(get_node("EndPanel/WinLossLabel"));
    change_label = Object::cast_to<Label>(get_node("EndPanel/ChangeLabel"));
    return_button = Object::cast_to<Button>(get_node("EndPanel/ReturnButton"));
    return_button->connect("pressed", this, "_pressed_return");
    //Label *debug_label = Object::cast_to<Label>(get_node("VBoxContainer/DebugLabel"));
    away_team = get_node("VBoxContainer/AwayTeam/HBoxContainer");
    TeamUnit *enemy_unit;
    Array enemy_ids = game_manager->enemy_ids;
    for (int i = 0; i < enemy_ids.size(); i++){
        Ref<PackedScene> unit = ResourceLoader::get_singleton()->load("res://Unit.tscn");
        Node *unit_instance = unit->instance();
        Object::cast_to<Unit>(unit_instance)->load(enemy_ids[i]);
        away_team->add_child(unit_instance);
        Object::cast_to<Unit>(unit_instance)->set_enemy_style();
    }
    String team_path = "/root/Game/" + String::num_int64(get_tree()->get_network_unique_id()) + "/VBoxContainer/VBoxContainer/Squad/HBoxContainer";
    Array team_units = get_node(NodePath(team_path))->get_children();
    home_team = get_node("VBoxContainer/HomeTeam/HBoxContainer");
    TeamUnit *team_unit;
    for (int i = 0; i < team_units.size(); i++){
        team_unit = Object::cast_to<TeamUnit>(team_units[i]);
        if (team_unit->get_child_count() > 0) {
            Ref<PackedScene> unit = ResourceLoader::get_singleton()->load("res://Unit.tscn");
            Node *unit_instance = unit->instance();
            Object::cast_to<Unit>(unit_instance)->load(Object::cast_to<Unit>(team_unit->get_child(0))->get_id());
            home_team->add_child(unit_instance);
        }
    }
    timer = Object::cast_to<Timer>(get_node("Timer"));
    timer->connect("timeout", this, "battle_tick");
    timer->start();
}

void Battlefield::battle_tick() {
    // TODO
    // change speed to 1.5, use game_manager, method in board to receive return | done
    // death animation?
    // update gold and health gui | done
    // have shop units & roll cost and require gold | done
    // design tiers - shop liklihood, cost. For each round, how much gold. Damage to player health
    // small known bug: if p1 ends turn while p2 is still in win screen, p2 end turn won't start battle
    // similar:         if p1 ends turn with empty team, p2 end turn won't start battle
    // gui for current tier
    // design animal with abilities
    // robust system to load in animals with different stats
    // mouseover on units to show descriptions
    Unit *home_unit;
    Unit *away_unit;
    if (home_team->get_child_count() > 0 && away_team->get_child_count() > 0){
        home_unit = Object::cast_to<Unit>(home_team->get_child(0));
        away_unit = Object::cast_to<Unit>(away_team->get_child(0));
        if (std::get<1>(home_unit->get_data()) <= 0 && std::get<1>(away_unit->get_data()) <= 0){
            home_team->remove_child(home_unit);
            away_team->remove_child(away_unit);
        }
        else if (std::get<1>(home_unit->get_data()) <= 0){
            home_team->remove_child(home_unit);
        }
        else if (std::get<1>(away_unit->get_data()) <= 0){
            away_team->remove_child(away_unit);
        }
        else {
            home_unit->take_damage(std::get<0>(away_unit->get_data()));
            away_unit->take_damage(std::get<0>(home_unit->get_data()));
            home_unit->home_attack_animate();
            away_unit->away_attack_animate();
        }
    }
    else if (home_team->get_child_count() == 0 && away_team->get_child_count() == 0){
        // draw
        end_panel->set_visible(true);
        win_loss_label->set_text("Draw :|");
        change_label->set_text("+4 gold\n");
        game_manager->gold += 4;
        timer->stop();
    }
    else if (away_team->get_child_count() == 0){
        // win
        end_panel->set_visible(true);
        win_loss_label->set_text("Victory :)");
        change_label->set_text("+4 gold\n+1 victory gold !");
        game_manager->gold += 5;
        timer->stop();
    }
    else {
        // loss
        int damage = 5;
        end_panel->set_visible(true);
        win_loss_label->set_text("Defeat :(");
        change_label->set_text("+4 gold\n-" + String::num_int64(damage) + " health");
        game_manager->gold += 4;
        game_manager->player_health -= damage;
        timer->stop();
    }
}

void Battlefield::_pressed_return() {
    Object::cast_to<Board>(get_parent())->return_from_battle();
    get_parent()->remove_child(this);
}