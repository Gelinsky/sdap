#ifndef BOARD_H
#define BOARD_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>
#include <TextureButton.hpp>
#include <Button.hpp>
#include <Label.hpp>
#include <Panel.hpp>

namespace godot {

class Board : public Control {
    GODOT_CLASS(Board, Control)

private:
    GameManager *game_manager;
    int gold;
    Label *gold_label;
    Label *health_label;
    Label *turn_label;
    Label *tier_label;
    TextureButton *roll;
    TextureButton *sell;
    Button *end_turn_button;
    Panel *end_turn_panel;
    Array enemy_team;

public:
    static void _register_methods();

    Board();

    void _init();

    void _ready();

    void _pressed_roll();

    void _pressed_sell();

    void _pressed_end_turn();

    void return_from_battle();

    void update_gui();

    void set_selected_unit(TeamUnit *unit);
};

}

#endif