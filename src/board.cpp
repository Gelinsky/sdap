#include "board.h"
#include "unit.h"
#include "team_unit.h"
#include "shop_item.h"
#include "game.h"

#include <PackedScene.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <random>

using namespace godot;

void Board::_register_methods() {
    register_method("_ready", &Board::_ready);
    register_method("_pressed_roll", &Board::_pressed_roll);
    register_method("_pressed_sell", &Board::_pressed_sell);
    register_method("_pressed_end_turn", &Board::_pressed_end_turn);
    register_method("return_from_battle", &Board::return_from_battle);
    register_method("update_gui", &Board::update_gui);
}

Board::Board() {
}

void Board::_init() {
}

void Board::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    game_manager->turn = 1;
    game_manager->current_tier = 1;
    game_manager->gold = 20;
    game_manager->player_health = 100;
    game_manager->selected_unit = nullptr;
    gold_label = Object::cast_to<Label>(get_node("VBoxContainer/VBoxContainer/GUI/HBoxContainer/GoldPanel/HBoxContainer/GoldLabel"));
    health_label = Object::cast_to<Label>(get_node("VBoxContainer/VBoxContainer/GUI/HBoxContainer/HealthPanel/HBoxContainer/HealthLabel"));
    turn_label = Object::cast_to<Label>(get_node("VBoxContainer/VBoxContainer/GUI/HBoxContainer/TurnPanel/TurnLabel"));
    tier_label = Object::cast_to<Label>(get_node("VBoxContainer/VBoxContainer/GUI/HBoxContainer/TierPanel/TierLabel"));
    update_gui();
    roll = Object::cast_to<TextureButton>(get_node("VBoxContainer/HBoxContainer/Actions/VBoxContainer/HBoxContainer/MarginContainer/Roll"));
    roll->connect("pressed", this, "_pressed_roll");
    sell = Object::cast_to<TextureButton>(get_node("VBoxContainer/HBoxContainer/Actions/VBoxContainer/HBoxContainer/MarginContainer2/Sell"));
    sell->connect("pressed", this, "_pressed_sell");
    end_turn_button = Object::cast_to<Button>(get_node("VBoxContainer/HBoxContainer/Actions/VBoxContainer/EndTurn"));
    end_turn_button->connect("pressed", this, "_pressed_end_turn");
    end_turn_panel = Object::cast_to<Panel>(get_node("EndTurnPanel"));
    _pressed_roll();
}

void Board::_pressed_roll() {
    if (game_manager-> gold >= 1){
        Array shop_items = get_node("VBoxContainer/HBoxContainer/Shop/HBoxContainer")->get_children();
        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<int> dist(1, game_manager->animal_count);
        for (int i = 0; i < shop_items.size(); i++){
            Ref<PackedScene> unit = ResourceLoader::get_singleton()->load("res://Unit.tscn");
            Node *unit_instance = unit->instance();
            int idx = dist(mt)-1;
            std::tuple<int, int, String, int> unit_data = game_manager->unit_database[idx];
            // Keep drawing random animals until you get one within the current tier
            while (std::get<3>(unit_data) > game_manager->current_tier){
                idx = dist(mt)-1;
                unit_data = game_manager->unit_database[idx];
            }
            Object::cast_to<Unit>(unit_instance)->load(idx);
            Object::cast_to<ShopItem>(shop_items[i])->fill(unit_instance);
    }
    game_manager->gold--;
    update_gui();
    }
}

void Board::_pressed_sell() {
    TeamUnit *su = game_manager->selected_unit;
    if (su != nullptr && su->get_child_count() > 0){
        su->remove_child(su->get_child(0));
        su = nullptr;
        game_manager->gold++;
        update_gui();
    }
}

void Board::_pressed_end_turn() {
    Array team_units = get_node("VBoxContainer/VBoxContainer/Squad/HBoxContainer")->get_children();
    Array team_ids;
    TeamUnit *team_unit;
    for (int i = 0; i < team_units.size(); i++){
        team_unit = Object::cast_to<TeamUnit>(team_units[i]);
        if (team_unit->get_child_count() > 0) {
            team_ids.append(Object::cast_to<Unit>(team_unit->get_child(0))->get_id());
        }
    }
    Object::cast_to<Game>(get_node("/root/Game"))->end_turn(team_ids);
    end_turn_button->set_text("waiting...");
    end_turn_panel->set_visible(true);
}

void Board::return_from_battle() {
    end_turn_button->set_text("end turn");
    game_manager->turn++;
    game_manager->current_tier++;
    game_manager->enemy_ids.clear();
    update_gui();
    end_turn_panel->set_visible(false);
    _pressed_roll();
}

void Board::update_gui() {
    gold_label->set_text(String::num_int64(game_manager->gold));
    health_label->set_text(String::num_int64(game_manager->player_health));
    turn_label->set_text("Turn: " + String::num_int64(game_manager->turn));
    tier_label->set_text("Tier: " + String::num_int64(game_manager->current_tier));
}