#ifndef MAINMENU_H
#define MAINMENU_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>
#include <Button.hpp>
#include <NetworkedMultiplayerENet.hpp>

namespace godot {

class MainMenu : public Control {
    GODOT_CLASS(MainMenu, Control)

private:
    GameManager *game_manager;
    Button *host_button;
    Button *join_button;
    NetworkedMultiplayerENet *net;

public:
    static void _register_methods();

    MainMenu();

    void _init();

    void _ready();

    void _pressed_host();

    void _pressed_join();

    void _peer_connected(int64_t id);
};

}

#endif