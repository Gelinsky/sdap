#include "shop_item.h"
#include "team_unit.h"
#include "board.h"
#include "unit.h"

#include <Position2D.hpp>
#include <Label.hpp>
#include <Button.hpp>
#include <PackedScene.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>

using namespace godot;

void ShopItem::_register_methods() {
    register_method("_ready", &ShopItem::_ready);
    register_method("_buy_friend", &ShopItem::_buy_friend);
    register_method("fill", &ShopItem::fill);
}

ShopItem::ShopItem() {
}

void ShopItem::_init() {
}

void ShopItem::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    this->connect("pressed", this, "_buy_friend");
}

void ShopItem::_buy_friend() {
    if (get_child_count() > 0 && game_manager->gold >= 3){
        // the unit in this shop
        Unit *child = Object::cast_to<Unit>(get_node("Unit"));
        // get the first available team spot to move to
        String path = "/root/Game/" + String::num_int64(get_tree()->get_network_unique_id()) + "/VBoxContainer/VBoxContainer/Squad/HBoxContainer";
        Array team_units = get_node(NodePath(path))->get_children();
        bool found = false;
        int i;
        for (i = 0; i < team_units.size() && !found; i++){
            if (Object::cast_to<TeamUnit>(team_units[i])->get_child_count() == 0){
                found = true;
            }
        }
        if (found){
            TeamUnit *teammate = Object::cast_to<TeamUnit>(team_units[i-1]);
            // create a new identical unit
            Ref<PackedScene> unit = ResourceLoader::get_singleton()->load("res://Unit.tscn");
            Node *unit_instance = unit->instance();
            Object::cast_to<Unit>(unit_instance)->load(child->get_id());
            // remove shop's unit and add new unit to team
            remove_child(child);
            teammate->add_child(unit_instance);
            game_manager->gold -= 3;
            String board_path = "/root/Game/" + String::num_int64(get_tree()->get_network_unique_id());
            Object::cast_to<Board>(get_node(NodePath(board_path)))->update_gui();
        }
    }
}

void ShopItem::fill(Node *unit) {
    if (get_child_count() > 0){
        remove_child(get_child(0));
    }
    add_child(unit);
}
