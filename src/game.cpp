#include "game.h"
#include "team_unit.h"
#include "unit.h"

#include <PackedScene.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>

using namespace godot;

void Game::_register_methods() {
    register_method("_ready", &Game::_ready);
    register_method("end_turn", &Game::end_turn);
    register_method("set_enemy_team", &Game::set_enemy_team, GODOT_METHOD_RPC_MODE_REMOTE);
}

Game::Game() {
}

void Game::_init() {
}

void Game::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    game_manager->player_ids.append(get_tree()->get_network_unique_id());
    int64_t player_id = game_manager->player_ids[0];
    Ref<PackedScene> player1 = ResourceLoader::get_singleton()->load("res://Board.tscn");
    auto player_inst1 = player1->instance();
    player_inst1->set_name(String::num_int64(player_id));
    player_inst1->set_network_master(player_id);
    add_child(player_inst1);

    player_id = game_manager->player_ids[1];
    Ref<PackedScene> player2 = ResourceLoader::get_singleton()->load("res://Board.tscn");
    auto player_inst2 = player2->instance();
    player_inst2->set_network_master(player_id);
    player_inst2->set_name(String::num_int64(player_id));
    add_child(player_inst2);
}

void Game::end_turn(Array unit_ids) {
    bool ready = false;
    if (!game_manager->enemy_ids.empty()){
        ready = true;
        Ref<PackedScene> bf = ResourceLoader::get_singleton()->load("res://Battlefield.tscn");
        Node *bf_instance = bf->instance();
        String path = String::num_int64(game_manager->player_ids[1]);
        get_node(NodePath(path))->add_child(bf_instance);
    }
    Array args;
    args.append(unit_ids);
    args.append(ready);
    int64_t opponent_id = game_manager->player_ids[0];
    rpc_id(opponent_id, "set_enemy_team", args);
}

void Game::set_enemy_team(Array unit_ids, bool ready) {
    game_manager->enemy_ids = unit_ids;
    if (ready){
        Ref<PackedScene> bf = ResourceLoader::get_singleton()->load("res://Battlefield.tscn");
        Node *bf_instance = bf->instance();
        String path = String::num_int64(game_manager->player_ids[1]);
        get_node(NodePath(path))->add_child(bf_instance);
    }
}