#ifndef SHOPITEM_H
#define SHOPITEM_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>
#include <Button.hpp>

namespace godot {

class ShopItem : public Control {
    GODOT_CLASS(ShopItem, Control)

private:
    GameManager *game_manager;
    Button *buy_button;

public:
    static void _register_methods();

    ShopItem();

    void _init();

    void _ready();

    void _buy_friend();

    void fill(Node *unit);
};

}

#endif