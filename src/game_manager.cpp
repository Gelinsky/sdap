#include "game_manager.h"

using namespace godot;

void GameManager::_register_methods() {
    register_method("_ready", &GameManager::_ready);
}

GameManager::GameManager() {
}

void GameManager::_init() {
    unit_database.push_back(std::make_tuple(1, 1, "Rooster", 1));
    unit_database.push_back(std::make_tuple(2, 5, "Turtle", 2));
    unit_database.push_back(std::make_tuple(4, 3, "Deer", 3));
    unit_database.push_back(std::make_tuple(4, 5, "Horse", 3));
    unit_database.push_back(std::make_tuple(7, 6, "Tiger", 4));
    unit_database.push_back(std::make_tuple(8, 5, "Shark", 4));
    unit_database.push_back(std::make_tuple(5, 12, "Elephant", 5));
    animal_count = unit_database.size();
}

void GameManager::_ready() {
}