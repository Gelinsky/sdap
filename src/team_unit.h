#ifndef TEAMUNIT_H
#define TEAMUNIT_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>

namespace godot {

class TeamUnit : public Control {
    GODOT_CLASS(TeamUnit, Control)

private:
    GameManager *game_manager;

public:
    static void _register_methods();

    TeamUnit();

    void _init();

    void _ready();

    void _selected_team_unit();
};

}

#endif