#include "team_unit.h"
#include "board.h"

#include <SceneTree.hpp>

using namespace godot;

void TeamUnit::_register_methods() {
    register_method("_ready", &TeamUnit::_ready);
    register_method("_selected_team_unit", &TeamUnit::_selected_team_unit);
}

TeamUnit::TeamUnit() {
}

void TeamUnit::_init() {
}

void TeamUnit::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    this->connect("pressed", this, "_selected_team_unit");
}

void TeamUnit::_selected_team_unit() {
    game_manager->selected_unit = this;
}