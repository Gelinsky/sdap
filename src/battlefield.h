#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include "game_manager.h"

#include <Godot.hpp>
#include <Control.hpp>
#include <Timer.hpp>
#include <Label.hpp>
#include <Panel.hpp>
#include <Button.hpp>

namespace godot {

class Battlefield : public Control {
    GODOT_CLASS(Battlefield, Control)

private:
    GameManager *game_manager;
    Panel *end_panel;
    Label *win_loss_label;
    Label *change_label;
    Button *return_button;
    Node *home_team;
    Node *away_team;
    Timer *timer;

public:
    static void _register_methods();

    Battlefield();

    void _init();

    void _ready();

    void battle_tick();

    void _pressed_return();
};

}

#endif