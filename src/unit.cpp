#include "unit.h"

#include <Position2D.hpp>
#include <Button.hpp>
#include <ResourceLoader.hpp>
#include <Texture.hpp>
#include <Ref.hpp>

using namespace godot;

void Unit::_register_methods() {
    register_method("_ready", &Unit::_ready);
    register_method("load", &Unit::load);
    register_method("get_id", &Unit::get_id);
    register_method("take_damage", &Unit::take_damage);
    register_method("update_health", &Unit::update_health);
    register_method("set_enemy_style", &Unit::set_enemy_style);
    register_method("home_attack_animate", &Unit::home_attack_animate);
}

Unit::Unit() {
}

void Unit::_init() {
    // unit_database.push_back(std::make_tuple(1, 1, "Rooster"));
    // unit_database.push_back(std::make_tuple(2, 5, "Turtle"));
    // unit_database.push_back(std::make_tuple(4, 3, "Deer"));
    // unit_database.push_back(std::make_tuple(4, 5, "Horse"));
    // unit_database.push_back(std::make_tuple(7, 6, "Tiger"));
    // unit_database.push_back(std::make_tuple(8, 5, "Shark"));
    // unit_database.push_back(std::make_tuple(5, 12, "Elephant"));
}

void Unit::_ready() {
    game_manager = get_node<GameManager>("/root/GameManager");
    animation_player = Object::cast_to<AnimationPlayer>(get_node("AnimationPlayer"));
    attack = std::get<0>(game_manager->unit_database[id]);
    health = std::get<1>(game_manager->unit_database[id]);
    name = std::get<2>(game_manager->unit_database[id]);
    tier = std::get<3>(game_manager->unit_database[id]);
    icon = Object::cast_to<TextureRect>(get_node("VBoxContainer/TextureRect"));
    icon->set_texture((Ref<Texture>)ResourceLoader::get_singleton()->load("res://assets/" + name + ".png"));
    attack_label = Object::cast_to<Label>(get_node("VBoxContainer/MarginContainer/HBoxContainer/AttackRect/AttackLabel"));
    attack_label->set_text(String::num_int64(attack));
    health_label = Object::cast_to<Label>(get_node("VBoxContainer/MarginContainer/HBoxContainer/HealthRect/HealthLabel"));
    health_label->set_text(String::num_int64(health));
}

void Unit::load(int id) {
    this->id = id;
}

int Unit::get_id() {
    return id;
}

std::tuple<int, int, String> Unit::get_data() {
    return std::make_tuple(attack, health, name);
}

void Unit::take_damage(int dmg) {
    health -= dmg;
}

void Unit::update_health() {
    health_label->set_text(String::num_int64(health));
}

void Unit::set_enemy_style() {
    icon->set_flip_h(false);
}

void Unit::home_attack_animate() {
    animation_player->play("HomeAttack");
}

void Unit::away_attack_animate() {
    animation_player->play("AwayAttack");
}